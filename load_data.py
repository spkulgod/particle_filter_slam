import numpy as np


class Load_data:
  def __init__(self,dataset):
    with np.load("Encoders%d.npz"%dataset) as data:
      self.encoder_counts = data["counts"] # 4 x n encoder counts
      self.encoder_stamps = data["time_stamps"] # encoder time stamps

    with np.load("Hokuyo%d.npz"%dataset) as data:
      self.lidar_angle_min = data["angle_min"] # start angle of the scan [rad]
      self.lidar_angle_max = data["angle_max"] # end angle of the scan [rad]
      self.lidar_angle_increment = data["angle_increment"] # angular distance between measurements [rad]
      self.lidar_range_min = data["range_min"] # minimum range value [m]
      self.lidar_range_max = data["range_max"] # maximum range value [m]
      self.lidar_ranges = data["ranges"].T     # range data [m] (Note: values < range_min or > range_max should be discarded)
      self.lidar_stamps = data["time_stamps"]  # acquisition times of the lidar scans
    
    with np.load("Imu%d.npz"%dataset) as data:
      self.imu_angular_velocity = data["angular_velocity"] # angular velocity in rad/sec
      self.imu_linear_acceleration = data["linear_acceleration"] # Accelerations in gs (gravity acceleration scaling)
      self.imu_stamps = data["time_stamps"]  # acquisition times of the imu measurements
  
    with np.load("Kinect%d.npz"%dataset) as data:
      self.disp_stamps = data["disparity_time_stamps"] # acquisition times of the disparity images
      self.rgb_stamps = data["rgb_time_stamps"] # acquisition times of the rgb images

