import numpy as np 
import load_data
import map_utils
import matplotlib.pyplot  as plt
import my_functions

np.set_printoptions(threshold=np.nan)
'''
all transformation translations in terms of grid units and not meters
'''
dset = 20
data = load_data.Load_data(dset)
lidar = my_functions.Lidar(data.lidar_angle_max,data.lidar_angle_min,
            data.lidar_angle_increment,data.lidar_range_max,data.lidar_range_min)
res = 0.05
prediction = my_functions.prediction(data,res)
map = my_functions.Map(40,res)
state = np.array([[500,1000,0]])
particle_filter = my_functions.particle_filter(np.shape(map.grid))
particle_filter.particles = np.append(particle_filter.particles , state, axis = 0)
update = my_functions.update(res,40)
lidar.data = prediction.get_lidar_data()
rgbd = my_functions.RGBD(data,dset,map.grid)
i=0
while i in range(np.size(data.encoder_stamps)):
    val = np.argmax(particle_filter.weights)
    state = particle_filter.particles[val]
    map.update_map(lidar,state)
    color_update_flag = \
                rgbd.color_map(state,data.encoder_stamps[prediction.enc_cnt])
    particle_filter.particles = prediction.next_state(particle_filter.particles)
    if(i%10 == 0):
        map.show(np.array(map.path),state,i)
        rgbd.show_color_map(state,map.grid)
    lidar.data = prediction.get_lidar_data()
    particle_filter.particles, particle_filter.weights \
                = update.weights(particle_filter.particles,
                particle_filter.weights,map,lidar)
    i = prediction.enc_cnt
    print(i)
map.show(np.array(map.path),state)
plt.show(block=True)
