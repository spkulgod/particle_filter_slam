import numpy as np
import map_utils
import matplotlib.pyplot  as plt
from scipy import signal
import cv2
import os
from PIL import Image

class Lidar:
    def __init__(self,max_a,min_a,inc_a,max_r,min_r):
        self.angles = np.arange(min_a,max_a+inc_a/10,inc_a)
        self.max_r = max_r
        self.min_r = min_r
        self.data = np.array([])

def norm(a,b,c):
    val = np.exp(-a*a - b*b - c*c)/np.sqrt(np.pi*2)
    return val
        
def transform_fun(theta,p):
    T = np.array([[np.cos(theta), -np.sin(theta),p[0]],[np.sin(theta),
            np.cos(theta),p[1]],[0,0,1]])
    return T

class Map:
    def __init__(self,sz,res):
        self.res = res
        p = np.array([0.13323,0])/self.res
        self.bTl = transform_fun(0,p)
        self.size = int(np.ceil(sz*2/self.res) + 1)
        self.grid = np.zeros((self.size,self.size),dtype = np.float16)
        self.binary=self.grid < 0
        #self.binary= 1/(1+np.exp(self.grid))
        self.lo = np.log(4)
        self.fig ,self.pl = plt.subplots()
        self.v_pos_lo = np.vectorize(self.pos_lo)
        self.v_neg_lo = np.vectorize(self.neg_lo)
        self.v_bresenham = np.vectorize(self.bresenham)
        self.path = []
 
    def show(self,particles,state,num):
        self.pl.cla()
        self.pl.imshow(self.grid.astype(int),cmap='gray')
        '''self.pl.plot(self.org[1],self.org[0],'+')
        self.pl.quiver(self.org[1],self.org[0],np.sin(self.org[2]),np.cos(self.org[2]))'''
        self.pl.scatter(particles[:,1],particles[:,0],s=10,marker='*',color = 'r')
        self.pl.scatter(state[1],state[0],s=10,marker='+')
        self.fig.savefig("dset4/map"+str(num)+".png")


    def pos_lo(self,x,y):
        if self.grid[x,y]<30:
            self.grid[x,y] = self.grid[x,y]+self.lo
    def neg_lo(self,x,y):
        if self.grid[x,y]>-30:
            self.grid[x,y] = self.grid[x,y]-self.lo

    def bresenham(self,a,b,c,d):     
        cells = map_utils.bresenham2D(c,d,a,b).astype(int)
        self.neg_lo(cells[0,-1],cells[1,-1])
        cells = cells[:,:-1]
        self.v_pos_lo(cells[0],cells[1])

        
    def update_map(self,lidar,state):
        indValid = np.logical_and((lidar.data< lidar.max_r),
                    (lidar.data> lidar.min_r))
        self.path.append(state)
        ranges = lidar.data[indValid]
        wTb = transform_fun(state[-1],state[:2])
        wTl = np.dot(self.bTl,wTb)
        self.org = np.dot(wTl,[0,0,1])
        self.org[2] = state[2]
        angles = lidar.angles[indValid]
        xs = ranges*np.cos(angles)/self.res
        ys = ranges*np.sin(angles)/self.res
        sz_valid = np.size(xs)
        one = np.ones(sz_valid)
        pos = np.array([xs,ys,one])
        pos = np.dot(wTl,pos)
        self.v_bresenham(pos[0,:],pos[1,:],self.org[0],self.org[1])
        self.binary = self.grid < -0.1



class prediction:
    def __init__(self,data,res):
        self.res = res
        self.data = data
        v_dist_calc = np.vectorize(self.dist_calc)
        self.dist = v_dist_calc(self.data.encoder_counts[0,:],
                    self.data.encoder_counts[1,:],
                    self.data.encoder_counts[2,:],
                    self.data.encoder_counts[3,:])
        b,a = signal.butter(1,0.01,'low')
        self.avel = signal.filtfilt(b,a,self.data.imu_angular_velocity[2,:])
        self.enc_cnt = 200
        self.imu_cnt = 200
        self.lidar_cnt = 200
        self.find_start()

    def dist_calc(self,a,b,c,d):
        dist = (a+b+c+d)/4*0.0022/self.res
        return dist

    def find_start(self):
        while(self.data.encoder_stamps[self.enc_cnt]>
                    self.data.imu_stamps[self.imu_cnt]):
            self.imu_cnt = self.imu_cnt+1
        while(self.data.encoder_stamps[self.enc_cnt]<
                    self.data.imu_stamps[self.imu_cnt]):
            self.enc_cnt = self.enc_cnt+1
        
    def next_input(self):
        i = 0.0000001
        ang_vel = 0
        self.enc_cnt = self.enc_cnt+1
        while (self.data.encoder_stamps[self.enc_cnt]>
                    self.data.imu_stamps[self.imu_cnt]):
            i = i+1
            ang_vel = ang_vel+self.avel[self.imu_cnt]
            self.imu_cnt = self.imu_cnt+1
        ang_vel = ang_vel/i
        dist = self.dist[self.enc_cnt]
        tau = (self.data.encoder_stamps[self.enc_cnt] - 
                    self.data.encoder_stamps[self.enc_cnt-1])

        return tau,dist,ang_vel
    
    def  next_state(self,prev_state):
        shape = np.shape(prev_state)
        tau,dist,ang_vel = self.next_input()
        t1 = np.array([dist*np.sinc(ang_vel/np.pi*tau/2)*np.cos(prev_state[:,2]+
                    ang_vel*tau/2)])
        t2 = np.array([dist*np.sinc(ang_vel/np.pi*tau/2)*np.sin(prev_state[:,2]+
                    ang_vel*tau/2)])
        t3 = np.array([tau*ang_vel*np.ones(shape[0])])
        new_state = np.add(prev_state,np.concatenate((t1.T,t2.T,t3.T),axis=1))
        noise = (new_state-prev_state)*0.5*np.random.randn(shape[0],shape[1])
        new_state = new_state+noise
        return new_state
    
    def get_lidar_data(self):
        while (self.data.encoder_stamps[self.enc_cnt]>
                    self.data.lidar_stamps[self.lidar_cnt]):
            self.lidar_cnt = self.lidar_cnt + 1
        return self.data.lidar_ranges[self.lidar_cnt-1]

class particle_filter:
    def __init__(self,shp):
        num = 20
        self.particles = np.random.rand(num,3)*np.array([shp[0],shp[1],np.pi*2])
        self.weights = np.ones(num+1)/(num+1)
        self.weights[num] = self.weights[num]+0.001

class update:
    def __init__(self,res,sz):
        self.res = res
        self.x_im = np.arange(0,2*sz+res,res)
        self.y_im = np.arange(0,2*sz+res,res)
        self.x_range = np.arange(-0.2/res,0.2/res+1,1)
        self.y_range = np.arange(-0.2/res,0.2/res+1,1)
        self.yaw_range = np.linspace(-np.pi/30,np.pi/30,9)
  
    def weights(self,particles,weights,map,lidar):
        self.num_particles = np.shape(particles)[0]
        corr_prob,particles = self.check_corelation(map,particles,lidar)
        temp = weights*corr_prob
        weights = temp/np.sum(temp)
        Neff = 1/np.sum(weights*weights)
        print(Neff)
        if (Neff <1.5):
            particles,weights=self.resample(particles,weights)
        return particles,weights
    
    def resample(self,particles,weights):
        sort_args = np.argsort(-weights)
        weights = -np.sort(-weights)
        temp = np.cumsum(weights)
        vals = np.linspace(0,1,num=self.num_particles,endpoint=False)
        u = np.random.rand(1)/self.num_particles
        vals = vals+u
        pos_temp = 0
        pos_vals = 0
        new_particles=[]
        while(pos_vals<self.num_particles):
            while(pos_vals<self.num_particles and vals[pos_vals]<temp[pos_temp]):
                new_particles.append(particles[sort_args[pos_temp]])
                pos_vals = pos_vals+1
            pos_temp = pos_temp+1
        weights = np.ones(self.num_particles)/self.num_particles
        return np.array(new_particles),weights

    def check_corelation(self,map,particles,lidar):
        indValid = np.logical_and((lidar.data< lidar.max_r),
                    (lidar.data> lidar.min_r))
        corr_mat = np.ones(self.num_particles)
        ranges = lidar.data[indValid]
        angles = lidar.angles[indValid]
        xs = ranges*np.cos(angles)/self.res
        ys = ranges*np.sin(angles)/self.res
        sz_valid = np.size(xs)
        one = np.ones(sz_valid)
        pos = np.array([xs,ys,one])
        for i in range(self.num_particles):
            c_stack = []
            for j in range(self.yaw_range.size):
                wTb = transform_fun(particles[i,-1]+self.yaw_range[j],
                            particles[i,:2])
                wTl = np.dot(map.bTl,wTb)
                Y = np.round(np.dot(wTl,pos))
                Y= Y.astype(int)
                c = map_utils.mapCorrelation(map.binary,self.x_im,self.y_im,Y[0:2,:],
                        self.x_range,self.y_range)
                c_stack.append(c)
            c_stack = np.array(c_stack)
            max_arg = np.unravel_index(np.argmax(c_stack),c_stack.shape)
            corr_mat[i] = c_stack[max_arg]/2
            max_arg = np.subtract(max_arg,(0,4,4))
            particles[i,:] = particles[i,:] + np.append(max_arg[1:3],#[0])
                        self.yaw_range[max_arg[0]])
        corr_mat = np.exp(corr_mat)
        corr_mat = corr_mat/sum(corr_mat)
        return corr_mat,particles


class RGBD:
    def __init__(self,data,num,map):
        self.rgbd_stamps = data.rgb_stamps
        self.depth_stamps = data.disp_stamps
        self.count = 1
        self.pathname = os.path.realpath(__file__)
        sub  = "my_functions.py"
        self.pathname = self.pathname[:-len(sub)]+"dataRGBD/"
        self.depth_path = self.pathname+"Disparity"+str(num)+"/disparity"+\
                str(num)+ "_"
        self.rgb_path = self.pathname+"RGB"+str(num)+"/rgb"+str(num) + "_"
        disp = cv2.imread(self.depth_path + str(self.count) + ".png",0)
        self.sz_depth = np.shape(disp)
        i = np.arange(0,self.sz_depth[1],1)
        j = np.arange(0,self.sz_depth[0],1)
        self.mesh = np.array(np.meshgrid(j,i)).T
        self.rgbmesh = np.array(np.meshgrid(j,i)).T
        self.rgbmesh[:,:,0] = (self.mesh[:,:,0]*526.37 + 16662.0)/585.051
        self.k = np.array([[585.05108211,0,242.94140713],[0,585.05108211
                    ,315.83800193],[0,0,1]])
        shape = np.shape(self.mesh)
        ones = np.ones((shape[0],shape[1],1))
        self.mesh = np.vstack((self.mesh.T,ones.T)).T
        self.mesh_unwound = np.reshape(self.mesh.T,(3,-1))
        self.v_image_bool = np.vectorize(self.image_bool)
        self.v_assign_color = np.vectorize(self.assign_colour)
        self.create_tf_mat()
        mp_shp = np.shape(map)
        self.rgb_map = np.zeros((mp_shp[0],mp_shp[1],3))
        self.fig , self.pl = plt.subplots()

    def create_tf_mat(self):
        Roc = np.array([[0,-1,0,0],[0,0,-1,0],[1,0,0,0],[0,0,0,1]]).T
        theta = 0.36
        psy = 0.021
        pitch = np.array([[np.cos(theta),0,np.sin(theta)],
                    [0,1,0], [-np.sin(theta),0,np.cos(theta)]])
        yaw = np.array([[np.cos(psy),-np.sin(psy),0],
                    [np.sin(psy),np.cos(psy),0],[0,0,1]])
        R = np.append(np.dot(pitch,yaw),np.array([[0,0,0]]),axis=0)
        P = np.array([[0.18,0.005,0.36,1]])
        bTc = np.append(R,P.T,axis=1)
        self.bTd = np.dot(bTc,Roc)

    def image_bool(self,a,b):
        self.image_bool[a,b]=True

    def load_next_images(self):
        self.rgb = plt.imread(self.rgb_path + str(self.count) + ".png")
        im = Image.open(self.depth_path + str(self.count) + ".png")
        self.count = self.count+1
        self.image_bool = np.zeros((480,640),dtype=bool)
        disp = np.array(im)
        disp2 = -0.00304*disp + 3.31
        depth = np.array(1.03/disp2)
        self.rgbmesh[:,:,1] = (self.mesh[:,:,1]*526.37 + disp2*
                    (-4.5*1750.46)+19276.0)/585.051
        depth_unwound = np.reshape(depth.T,(1,-1))
        pos_cam = np.dot(np.linalg.inv(self.k),self.mesh_unwound)
        pos_cam2 = np.zeros(np.shape(pos_cam))
        pos_cam2[0,:] = pos_cam[1,:]*depth_unwound
        pos_cam2[1,:] = pos_cam[0,:]*depth_unwound
        pos_cam2[2,:] = pos_cam[2,:]*depth_unwound
        pos_cam2 = np.vstack((pos_cam2,np.ones(307200)))
        state_floor = np.dot(self.bTd,pos_cam2)
        state_floor = np.reshape(state_floor,(4,640,480)).T
        floor_bool = state_floor[:,:,2]<-0.13
        img_posns = np.zeros(np.shape(self.rgbmesh))
        img_posns[:,:,1] = self.rgbmesh[:,:,1]*floor_bool
        img_posns[:,:,0] = self.rgbmesh[:,:,0]*floor_bool
        self.img_posns = img_posns.astype(int)
        self.v_image_bool(self.img_posns[:,:,0],self.img_posns[:,:,1])
        self.pos_floor = np.ones((480,640,3))
        self.pos_floor[:,:,0] = state_floor[:,:,0]*floor_bool/0.05
        self.pos_floor[:,:,1] = state_floor[:,:,1]*floor_bool/0.05
        self.rgb[:,:,0] = self.rgb[:,:,0]*self.image_bool
        self.rgb[:,:,1] = self.rgb[:,:,1]*self.image_bool
        self.rgb[:,:,2] = self.rgb[:,:,2]*self.image_bool
        fig2 ,pl2 = plt.subplots()
        pl2.imshow(state_floor[:,:,2])
        fig3 ,pl3 = plt.subplots()
        pl3.imshow(self.rgb)
        plt.show(block=True)

    def assign_colour(self,x,y,u,v):
        try:
            self.rgb_map[x,y,:] = self.rgb[u,v,:]
        except:
            print("bounds")

    def color_map(self,state,enc_t_stamp):
        if ((enc_t_stamp-self.rgbd_stamps[self.count])**2<0.015**2):
            self.load_next_images()
            wTb = transform_fun(state[-1],state[:2])
            pos_floor_unwound = np.dot(wTb,np.reshape(self.pos_floor.T,(3,-1)))
            img_posns_unwound = np.reshape(self.img_posns.T,(2,-1))
            pos_floor_unwound = pos_floor_unwound.astype(int)
            self.v_assign_color(pos_floor_unwound[0,:],pos_floor_unwound[1,:],
                        img_posns_unwound[0,:],img_posns_unwound[1,:])
            return 1
        while((enc_t_stamp>self.rgbd_stamps[self.count]) and 
                    ((enc_t_stamp-self.rgbd_stamps[self.count])**2>0.015**2)):
            self.count = self.count+1
        return 0
    
    def show_color_map(self,state,grid):
        bw_map = np.array([grid.T>0.1]*3).T
        rgb_img = bw_map*self.rgb_map
        map = 1-1/(1+np.exp(grid.T))
        act_map = np.array([map]*3).T 
        rgb_bw = rgb_img>0
        act_map = act_map - act_map*rgb_bw + rgb_img
        act_map = act_map.astype(float)
        self.pl.cla()
        self.pl.imshow(act_map)
        '''self.pl.plot(self.org[1],self.org[0],'+')
        self.pl.quiver(self.org[1],self.org[0],np.sin(self.org[2]),np.cos(self.org[2]))'''
        self.pl.scatter(state[1],state[0],s=10,marker='+')
        self.fig.savefig("dset2/rgbmap"+str(self.count)+".png")
        self.pl.cla()
        self.pl.imshow(rgb_img)
        self.fig.savefig("dset2/rgb"+str(self.count)+".png")






