#### Description:

This report presents an approach of implementing simultaneous localization and mapping(SLAM) using particle filters and coloring the floor of the 2-D occupancy grid map. In this work, the IMU data and odometry data of a differential drive robot are used for yaw calculations, movement evement estimations for the robot. A particle filter containing 100 particles is used for prediction and updating of the robots position. Lidar readings are used to generate map and to estimate the best position of the robot in the map. Depth data obtained from a RGBD  camera(Kinect) is used to determine the floor region and that region is coloured in the map using the corresponding RGB values of the coloured image.

#### Contents:  

load_data - file used to load the dataset values into classes.  
map_utils - file used for bresenham and correlation value calculation.  
my_functions - file consisting of all the functions required for SLAM execution.  
my_program - file containing the main loop which controls the flow of the program.  

### Results:

###### dataset 1:
![Alt text](gifs/gif1.gif "mapping of dataset 21")
![Alt text](gifs/gif2.gif "mapping of dataset 21 with rgb overlay")

###### dataset 2:
![Alt text](gifs/gif3.gif "mapping of dataset 20")
![Alt text](gifs/gif4.gif "mapping of dataset 20 with rgb overlay")

